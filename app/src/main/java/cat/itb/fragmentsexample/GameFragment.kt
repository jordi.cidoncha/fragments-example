package cat.itb.fragmentsexample

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

const val RESULT_KEY = "RESULT"

class GameFragment : Fragment(R.layout.fragment_game), View.OnClickListener {
    // UI Views
    private lateinit var carta1: ImageView
    private lateinit var carta2: ImageView
    private lateinit var carta3: ImageView
    private lateinit var carta4: ImageView
    private lateinit var carta5: ImageView
    private lateinit var carta6: ImageView
    private lateinit var resetButton: Button

    // ViewModel declaration
    private val viewModel: GameViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        carta1 = view.findViewById(R.id.carta1)
        carta2 = view.findViewById(R.id.carta2)
        carta3 = view.findViewById(R.id.carta3)
        carta4 = view.findViewById(R.id.carta4)
        carta5 = view.findViewById(R.id.carta5)
        carta6 = view.findViewById(R.id.carta6)
        resetButton = view.findViewById(R.id.reset_button)

        carta1.setOnClickListener(this)
        carta2.setOnClickListener(this)
        carta3.setOnClickListener(this)
        carta4.setOnClickListener(this)
        carta5.setOnClickListener(this)
        carta6.setOnClickListener(this)

        resetButton.setOnClickListener {
            viewModel.resetEstatJoc()
            updateUI()
        }

        updateUI()
    }

    override fun onClick(v: View?) {
        when (v) {
            carta1 -> girarCarta(0, carta1)
            carta2 -> girarCarta(1, carta2)
            carta3 -> girarCarta(2, carta3)
            carta4 -> girarCarta(3, carta4)
            carta5 -> girarCarta(4, carta5)
            carta6 -> girarCarta(5, carta6)
        }
        if (viewModel.finalJoc()) {
            val action = GameFragmentDirections.actionGameToResult(viewModel.calcularResultat())
            findNavController().navigate(action)
        }
    }



    // Funció que utilitzarem per girar la carta
    private fun girarCarta(idCarta: Int, carta: ImageView) {
        carta.setImageResource(viewModel.girarCarta(idCarta))
    }

    // Funció que restauarà l'estat de la UI
    private fun updateUI() {
        carta1.setImageResource(viewModel.estatCarta(0))
        carta2.setImageResource(viewModel.estatCarta(1))
        carta3.setImageResource(viewModel.estatCarta(2))
        carta4.setImageResource(viewModel.estatCarta(3))
        carta5.setImageResource(viewModel.estatCarta(4))
        carta6.setImageResource(viewModel.estatCarta(5))

    }
}












