package cat.itb.fragmentsexample

import androidx.lifecycle.ViewModel

class GameViewModel: ViewModel() {
    // Game data model
    private var imatges = arrayOf(
        R.drawable.card1,
        R.drawable.card2,
        R.drawable.card3,
        R.drawable.card1,
        R.drawable.card2,
        R.drawable.card3
    )
    private var cartes = mutableListOf<Carta>()
    private var movements = 0

    // Aquesta funció es estàndard de cada classe, i és com el constructor

    init {
        setDataModel()
    }

    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    private fun setDataModel() {
        imatges.shuffle()
        for (i in 0..5) {
            cartes.add(Carta(i, imatges[i]))
        }
    }

    // Funció que comprova l'estat de la carta, el canvia i envia
    // a la vista la imatge que s'ha de pintar
    fun girarCarta(idCarta: Int) : Int {
        movements++
        if (!cartes[idCarta].girada) {
            cartes[idCarta].girada = true
            return cartes[idCarta].resId
        } else {
            cartes[idCarta].girada = false
            return R.drawable.back
        }
    }

    fun finalJoc(): Boolean {
        for (i in 0..5) {
            if (!cartes[i].girada) return false
        }
        return true
    }

    // Funció que posa l'estat del joc al mode inicial
    fun resetEstatJoc() {
        cartes = mutableListOf<Carta>()
        movements = 0
        setDataModel()
    }

    // Funció que retorna l'estat actual d'una carta
    fun estatCarta(idCarta: Int): Int {
        if(cartes[idCarta].girada) return cartes[idCarta].resId
        else return R.drawable.back
    }

    fun calcularResultat(): Int {
        return 100 - (movements-6)*10
    }
}

