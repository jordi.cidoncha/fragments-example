package cat.itb.fragmentsexample

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController

class ResultFragment : Fragment(R.layout.fragment_result) {
    private lateinit var resultTextView: TextView
    private lateinit var playButton: Button
    private lateinit var menuButton: Button

    private val viewModel: GameViewModel by activityViewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        resultTextView = view.findViewById(R.id.result_textview)
        playButton = view.findViewById(R.id.play_button)
        menuButton = view.findViewById(R.id.menu_button)

        resultTextView.setText(arguments?.getInt("result").toString())

        playButton.setOnClickListener {
            viewModel.resetEstatJoc()
            findNavController().navigate(R.id.action_result_to_game)
        }

        menuButton.setOnClickListener {
            findNavController().navigate(R.id.action_result_to_menu)
        }
    }

}